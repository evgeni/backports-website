Now that buster was released we are pleased to announce the availability of
buster-backports and stretch-backports-sloppy. 

## What to upload where 

 As a reminder, uploads to a release-backports pocket are to be taken
from release + 1, uploads to a release-backports-sloppy pocket are to be
taken from release + 2. Which means:

 Source Distribution | Backports Distribution | Sloppy Distribution
---------------------|------------------------|-------------------------
buster               | stretch-backports      | -
bullseye             | buster-backports       | stretch-backports-sloppy

## Backports and LTS 

Please keep in mind that backports doesn't follow LTS. Which means that we will drop
support for oldstable (stretch) around one year after the release of buster. Thats in sync
with the - official - security support for [oldstable](https://www.debian.org/security/faq#lifespan)

## BSA Security Advisories 

We plan to switch the security-announce mailinglist to keyring based authentication, which means that
every DD and DM is able to publish its own BSA advisories. We will send out a seperate announcement
after the switch happened - and of course update the [documentation](https://backports.debian.org/Contribute/#index4h2)

## Statistics

 For packages backported from buster, so far we have 1624 different
source packages in stretch-backports. Those 1624 source packages took 2821 uploads
from 252 uploaders to become reality.

## Thanks

 Thanks have to go out to all people making backports possible, and that
includes up front the backporters themself who do upload the packages,
track and update them on a regular basis, but also the buildd team
making the autobuilding possible and the ftp masters for creating the
suites in the first place.

Happy Backporting!

Alex and Rhonda - backports.debian.org ftpmasters

